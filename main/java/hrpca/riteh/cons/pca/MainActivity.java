package hrpca.riteh.cons.pca;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.location.LocationProvider;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Used for logging
    private static final String TAG = "ReportActivity";

    private SendReportTask sendReport;

    private Context context;

    private TextView infoText = null;

    private String jsonReceived = "";
    private ArrayList<Report> reportDataList = new ArrayList<Report>();

    private LocationManager mLocationManager;
    private Location mCurrentLocation;
    private int secondsGPSRefresh = 5;  // TODO implement setting of GPS refresh over settings
    private GeoManager geomngr;
    private PCANotificator notifMngr;
    // Fetcher thread objects
    private NewDataFetcher dataFetcherThread;
    private Thread fThread;

    // Used for the visual alarm notification
    private AnimationDrawable bgToggler;

    private SharedPreferences prefs;

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            Log.d(TAG, "Location changed. New location: " + location.toString());
            mCurrentLocation = location;
            //infoText.setText("Longitude:" + Double.toString(location.getLongitude()) + ", Latitude:" + Double.toString(location.getLatitude()) + ", Accuracy: " + Double.toString(location.getAccuracy()));


            // Initiate notifications according to new location received
            boolean flashnotif = prefs.getBoolean("visual_notification", false);
            boolean notification = prefs.getBoolean("notifications_new_message", true);

            // Handle notifications if phone is in the "hot zone"
            if (geomngr.isInHozZone(location)) {
                notifMngr.notification();
                if (flashnotif && notification) {
                    backgroundFlashing(true);
                }
                else {
                    backgroundFlashing(false);
                }
            } else {
                notifMngr.cancelAll();
                backgroundFlashing(false);
            }

            // Start the fetcher thread if not already running
            if (!dataFetcherThread.isRunning()) {
                Log.d(TAG, "Start Fetcher Thread");
                fThread = new Thread(dataFetcherThread);
                fThread.start();
            }
        }

        @Override
        public void onProviderDisabled(String string) {
            Log.e(TAG, "Provider disabled: "+string);
            infoText.setText("Not connected to location services");
        }


        @Override
        public void onProviderEnabled(String string) {
            Log.d(TAG, "Provider enabled: "+string);
            //infoText.setText("Connected to location services");
        }

        @Override
        public void onStatusChanged(String string, int status, Bundle bundle) {
            Log.d(TAG, "Status changed: "+string+" - "+Integer.toString(status));

            if (status == LocationProvider.AVAILABLE) {
                Log.d(TAG, "Available");
                infoText.setText("GPS OK");
            }

            if (status == LocationProvider.OUT_OF_SERVICE) {
                Log.d(TAG, "Out of service");
                infoText.setText("GPS  out of service");
            }

            if (status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
                Log.d(TAG, "Temporarily unavailable");
                infoText.setText("GPS  temporarily unavailable");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "On Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        infoText = (TextView) findViewById(R.id.infoText);
        TextView distText = (TextView) findViewById(R.id.distText);
        context = this;
        geomngr = new GeoManager(this, distText);
        notifMngr = new PCANotificator(this);
        dataFetcherThread = new NewDataFetcher(this, geomngr);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        Log.d(TAG, "All preferences: "+prefs.getAll().toString());


        // Initialize background flashing alarm notification
        View parent = findViewById(android.R.id.content);
        if (parent == null) {
            Log.d(TAG, "PARENT IS NULL!!!");
        }
        bgToggler = (AnimationDrawable) getResources().getDrawable(R.drawable.animation_background);
        parent.setBackgroundDrawable(bgToggler);

        // Set the background color to white
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(Color.WHITE);

        Button reportButton = (Button) findViewById(R.id.reportButton);
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Report button clicked!");
                if (mCurrentLocation != null) {
                    sendReport = new SendReportTask(mCurrentLocation.getLongitude(), mCurrentLocation.getLatitude());
                    sendReport.execute();
                } else {
                    Log.d(TAG, "No location has been collected yet - aborting report sending");
                    infoText.setText("No location found to send!");
                    notifMngr.cancelAll();
                }
            }
        });

        Button closeButton = (Button) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Close button clicked!");
                notifMngr.cancelAll();
                dataFetcherThread.interrupt();
                //fThread.interrupt();
                finish();
                System.exit(0);
            }
        });

        Button settingsButton = (Button)  findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Settings button clicked!");
                Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                context.startActivity(settingsIntent);
            }
        });

        Button logoutButton = (Button) findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "LogOut button clicked!");
                logOut();
            }
        });


        // Get location service and request periodic location updates
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mCurrentLocation = null;
        try {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, secondsGPSRefresh*1000,
                    0, mLocationListener);
            //mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, mLocationListener, null);
        } catch(SecurityException e) {
            Log.e(TAG, "Could not request location updates. Error: "+e.getMessage());
        } catch(Exception e) {
            Log.e(TAG, "Could not request location updates. Error: " + e.getMessage());
        }


        // Start the background task to fetch all active reports from the server
        FetchAllReportsTask fetchTask = new FetchAllReportsTask();
        fetchTask.execute((Void) null);

        // Check if GPS provider is enabed. If it is not, prompt the user to turn on location services
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(context.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(context.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(context.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }
    }

    // Async task used to fetch all active data about reports/alerts
    public class FetchAllReportsTask extends AsyncTask<Void, Void, Boolean> {
        // Constructor
        FetchAllReportsTask() {
            Log.d(TAG, "Starting fetch data task!");
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            // Used to get the full response from the server
            StringBuffer response = new StringBuffer();
            response.append("");
            String urlString = "http://cons.riteh.hr/pca/pcaapi.php";
            ArrayList<Report> collectedReports = new ArrayList<Report>();

            OutputStream out;
            try {

                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                try {
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    String inputLine;
                    response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                } finally {
                    urlConnection.disconnect();
                }

                urlConnection.connect();

            } catch (Exception e) {
                System.out.println(e.getMessage());
                Log.e(TAG, "Exception occurred during network call. "+e.getMessage());
                infoText.setText("Internet not accessible! Cannot collect alert data.");
                return false;
            }
            jsonReceived = response.toString();

            collectedReports = interpretJSONReports(jsonReceived);
            reportDataList = collectedReports;
            geomngr.populateReportPoints(collectedReports);

            Log.i(TAG, "Received : "+jsonReceived);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if (success) {
                Log.d(TAG, "Succedded in fetching alerts data!");
                infoText.setText(R.string.got_initial_data);
            } else {
                Log.e(TAG, "Failed in fetching alerts data!");
                infoText.setText(R.string.failed_initial_data);
            }
        }

        @Override
        protected void onCancelled() {
            Log.e(TAG, "Task cancelled!");
        }
    }

    public class SendReportTask extends AsyncTask<Void, Void, Boolean> {

        private double lat;
        private double lon;
        private String user;
        private String token;

        SendReportTask(double longitude, double latitude) {
            lat = latitude;
            lon = longitude;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String urlString = "http://cons.riteh.hr/pca/pcaapi.php";  //params[0]; // URL to call

            // Collect session/user data - token and username
            SharedPreferences settings;
            settings = getSharedPreferences("session", MODE_PRIVATE);
            user = settings.getString("loggedin", "0");
            token = settings.getString("token", "0");
            Log.d(TAG, "Sending report from user: "+user);

            String data = "action=report&user="+user+"&lat="+Double.toString(lat)+"&lon="+Double.toString(lon); //params[1]; //data to post

            Log.d(TAG, "Sending new report: Latitude: "+Double.toString(lat)+" , Longitude: "+Double.toString(lon));

            // Used to get the full response from the server
            StringBuffer response;

            OutputStream out;
            try {

                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                out = new BufferedOutputStream(urlConnection.getOutputStream());
                BufferedWriter writer = new BufferedWriter (new OutputStreamWriter(out, "UTF-8"));

                writer.write(data);
                writer.flush();
                writer.close();
                out.close();

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                urlConnection.connect();

                Log.d(TAG, "Server response data: "+response.toString());

            } catch (Exception e) {
                System.out.println(e.getMessage());
                Log.e(TAG, "Exception occurred during sending of now report. "+e.getMessage());
            }

            // TODO: React to the response received
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            //showProgress(false);

            if (success) {
                infoText.setText("Report sent successfully");
                Log.d(TAG,"Report sent successfully");
            } else {
                infoText.setText("Report sending failed");
                Log.d(TAG,"Report sending failed");
            }
        }

        @Override
        protected void onCancelled() {
            Log.d(TAG, "Cancelled the sending of alert");
        }
    }

    // Used to collect all report data from the JSON string input and create/return a list of Report objects.
    public ArrayList<Report> interpretJSONReports(String input) {

        JSONArray jArray;

        try {
            jArray = new JSONArray(input);
        } catch (JSONException e) {
            Log.e(TAG,"Failed to parse input! Aborting decoding JSON");
            return null;
        }

        ArrayList<Report> result = new ArrayList<Report>();

        for (int i=0; i < jArray.length(); i++) {
            try {
                JSONObject oneObject = jArray.getJSONObject(i);
                // Pulling items from the array
                String latitude = oneObject.getString("lat");
                String longitude = oneObject.getString("lon");
                String time = oneObject.getString("time");

                Log.d(TAG, "Extracted from JSON-> lat:" + latitude + ", lon:" + longitude + ", time:" + time);

                result.add(new Report(longitude, latitude, time));

            } catch (JSONException e) {
                Log.e(TAG, "Failed to parse input");
            }
        }
        return result;
    }

    public void backgroundFlashing(boolean on) {
        if (on) {
            bgToggler.start();
        } else {
            bgToggler.stop();
            View view = this.getWindow().getDecorView();
            view.setBackgroundColor(Color.WHITE);
            View parent = findViewById(android.R.id.content);
            bgToggler = (AnimationDrawable) getResources().getDrawable(R.drawable.animation_background);
            parent.setBackgroundDrawable(null);
            parent.setBackgroundDrawable(bgToggler);
        }
    }

    public void logOut() {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        // Clearing the current session data - logged in username and token
        settings = getSharedPreferences("session", MODE_PRIVATE);
        editor = settings.edit();
        editor.putString("loggedin", "0");
        editor.putString("token", "0");
        editor.commit();
        Log.d(TAG,"Cleared token("+settings.getString("token", "x")+") and username("+settings.getString("loggedin", "x")+") from memory");

        Intent logoutIntent = new Intent(getApplicationContext(), LoginActivity.class);
        logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notifMngr.cancelAll();
        context.startActivity(logoutIntent);
        finish();
    }
    /*@Override
    public void onWindowFocusChanged(boolean gained) {
        super.onWindowFocusChanged(gained);
        if (gained) {
            bgToggler.start();
        }
    }*/

    // Used to avoid destroying of app with back button - which further avoids multiple app instances running
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.d(TAG, "Back button pressed!");
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_HOME);
            startActivity(i);
            return super.onKeyDown(KeyEvent.KEYCODE_HOME, event);
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onPause() {
        super.onPause();
        //mLocationManager.removeUpdates(mLocationListener);
        Log.d(TAG, "On Pause! Removed location listener");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "On Stop!");
        //notifMngr.cancelAll();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(mLocationListener);
        Log.d(TAG, "On Destroy!");
        notifMngr.cancelAll();
    }
}
