package hrpca.riteh.cons.pca;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by Filip Jurada on 12.2.2018..
 */

public class GeoManager {

    private static final String TAG = "GeoManager";
    protected ArrayList<Report> reportList;
    private boolean collectedReports;
    private double hotZoneRadius;
    private TextView infoText;
    private Context mContext;
    public int expireHours;
    private SharedPreferences prefs;

    public GeoManager(Context context) {
        Log.d(TAG, "Initialized GeoManager");
        reportList = new ArrayList<Report> ();
        collectedReports = false;
        hotZoneRadius = 2000;
        expireHours = 6;
        mContext = context;
    }

    public GeoManager(Context context, TextView tv) {
        Log.d(TAG, "Initialized GeoManager");
        reportList = new ArrayList<Report> ();
        collectedReports = false;
        hotZoneRadius = 2000;
        infoText = tv;
        expireHours = 6;
        mContext = context;
    }

    private void outputText(String str) {
        if (infoText != null) {
            infoText.setText(str);
        }
    }

    // Adding data points
    public void populateReportPoints(ArrayList<Report> reports) {
        if (reports != null) {
            for (Report r : reports) {
                reportList.add(r);
                Log.d(TAG, "Added report to list!");
            }
            collectedReports = true;
        } else {
            Log.d(TAG, "No reports provided - parameter is null");
        }
    }

    /*public void addReportPoint(LatLng entry) {
        reportList.add(new Report(entry.longitude, entry.latitude));
    }*/

    public void addReportPoint(Report report) {
        reportList.add(report);
    }

    public boolean isInHozZone(Location location) {
        double distance = -1;
        double minDistance = 9999999;
        boolean result = false;

        // Get hot zone radius from settings
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        hotZoneRadius = Integer.parseInt(prefs.getString("alerts_radius", "1000"));
        Log.d(TAG, "Alert zone radius : "+prefs.getString("alerts_radius", "1000"));

        // Loop through report list with iterator, and delete entries that are expired and check the distance to each point
        for (Iterator<Report> iter = reportList.listIterator(); iter.hasNext(); ) {
            Report r = iter.next();

            // Delete if expired
            if (isExpired(r.datetime)) {
                Log.i(TAG, "Report filed at: "+r.datetime.toString()+", has expired. Removing...");
                iter.remove();
                continue;
            }

            // Calculate distance to point
            distance = measureDistance(r, new Report(location.getLongitude(), location.getLatitude()));
            if (distance < minDistance) {
                minDistance = distance;
            }
        }

        // Check if current point is in hot zone
        if ( minDistance < hotZoneRadius ) {
            result = true;
        }

        // IF no distance has been measured, there are no alerts in the reportList. Otherwise, write down the lowest distance
        if (distance == -1) {
            Log.d(TAG, "There are no alert points!");
            outputText("No alerts available");
        } else {
            Log.d(TAG, "User is in the Hot Zone! Distance: "+Double.toString(minDistance));
            outputText("Closest alert point:\n"+Long.toString(Math.round(minDistance))+"m");
        }

        // Return the result of the operation
        return result;
    }

    // Formula used to calculate distance between two latitude/longitude coordinates
    private double measureDistance(Report r1, Report r2) {
        double lat1 = r1.latitude;
        double lon1 = r1.longitude;
        double lat2 = r2.latitude;
        double lon2 = r2.longitude;
        double R = 6378.137; // Radius of earth in KM
        double dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
        double dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                            Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
        return d * 1000; // meters
    }

    private boolean isExpired(Date dateToCheck) {
        Date now = new Date();
        long diffInMillis = now.getTime() - dateToCheck.getTime();
        // Millisecond to hour
        int diff = (int) diffInMillis/3600000;
        return (diff > expireHours);
    }

    public Date getNewestReportTime() {
        Date newestDate = new Date(Long.MIN_VALUE);

        if (reportList.isEmpty()) {
            return new Date(0);
        }

        for (Report r : reportList) {
            if (r.datetime.after(newestDate)) {
                newestDate = r.datetime;
            }
        }
        Log.d(TAG, "Newest report found at: "+newestDate.toString());
        // Increment the seconds to avoid receiving the identical report than the newest one
        newestDate.setTime(newestDate.getTime() + 1000);
        return newestDate;
    }
}
