package hrpca.riteh.cons.pca;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Filip Jurada on 2/1/2018.
 */

public class Report {
    public double longitude;
    public double latitude;
    public Date datetime;

    private static final String TAG = "ReportClass";

    // Constructor in case correct data is provided
    Report(double _lon, double _lat, Date _time) {
        longitude = _lon;
        latitude = _lat;
        datetime = _time;
    }

    // If no Date is provided, current time is assumed
    Report(double _lon, double _lat) {
        Date now = new Date();
        //Log.d(TAG, "No time provided. Assuming current time...");

        longitude = _lon;
        latitude = _lat;
        datetime = now;
    }

    // Constructor in case there is String data available
    Report(String _lon, String _lat, String _time) {
        longitude = Double.parseDouble(_lon);
        latitude = Double.parseDouble(_lat);

        // TODO? - Check if the inserted string is correct
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            datetime = format.parse(_time);
        } catch (Exception e) {
            Log.e(TAG, "Failed parsing of date string: "+e.getMessage());
        }

        Log.d(TAG, "Report added! Timestring: "+_time+", Datetime: "+datetime.toString());
    }
}
