package hrpca.riteh.cons.pca;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Filip on 15.2.2018..
 */

public class NewDataFetcher implements Runnable {

    private static final String TAG = "FetchThread";
    private volatile boolean running = false;
    private volatile MainActivity context;
    private volatile GeoManager geomngr;
    private  int sleepSeconds;
    private volatile SharedPreferences prefs;

    public NewDataFetcher(MainActivity _main, GeoManager _geomngr) {
        context = _main;
        geomngr = _geomngr;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        sleepSeconds = Integer.parseInt(prefs.getString("sync_frequency", "30"));
    }

    @Override
    public void run() {

        running = true;
        // Sleep initially
        try {
            sleepSeconds = Integer.parseInt(prefs.getString("sync_frequency", "30"));
            Thread.sleep(sleepSeconds * 1000);
        } catch (InterruptedException e) {
            Log.d(TAG, "Cannot sleep in thread - exception");
        }

        while (running) {
            try {
                // Get newest sleep time setting, fetch data, then sleep for that amount
                sleepSeconds = Integer.parseInt(prefs.getString("sync_frequency", "30"));
                FetchNewerReports(geomngr.getNewestReportTime());
                Log.d(TAG, "Thread sleeping for "+Integer.toString(sleepSeconds)+" Seconds");
                Thread.sleep(sleepSeconds*1000);
            } catch (InterruptedException e) {
                if(!running){
                    break;
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception: "+e.getMessage());
            }
        }
    }

    // Used to stop the thread
    public void interrupt() {
        Log.d(TAG, "Stopping the fetcher thread!");
        running = false;
    }

    public boolean isRunning() {
        return running;
    }


    private boolean FetchNewerReports(Date time) {
        String dateString;
        String urlString;
        String jsonReceived;
        StringBuffer response = new StringBuffer();

        // Prepare the date string for sending via GET request
        SimpleDateFormat stdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        dateString = stdFormat.format(time).replaceAll(" ", "%20");

        response.append("");
        urlString = "http://cons.riteh.hr/pca/pcaapi.php?action=getNewerThan&time="+dateString+"";
        Log.d(TAG, "Request URL: "+urlString);
        ArrayList<Report> collectedReports;


        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            try {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } finally {
                urlConnection.disconnect();
            }

            urlConnection.connect();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            Log.e(TAG, "Exception occurred during network call. "+e.getMessage());
            return false;
        }
        jsonReceived = response.toString();

        collectedReports = context.interpretJSONReports(jsonReceived);
        geomngr.populateReportPoints(collectedReports);

        Log.i(TAG, "Received : "+jsonReceived);
        return true;
    }
}
