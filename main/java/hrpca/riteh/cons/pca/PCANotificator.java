package hrpca.riteh.cons.pca;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import java.io.LineNumberReader;

/**
 * Created by Filip on 13.2.2018..
 *
 */

public class PCANotificator {

    private Context mContext;
    private NotificationManager notMngr;
    private NotificationCompat.Builder mBuilder;
    private SharedPreferences prefs;
    private PendingIntent pIntent;

    PCANotificator(Context context) {

        mContext = context;

        Intent resetIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        resetIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        pIntent = PendingIntent.getActivity(
                context,
                0,
                resetIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        // Get preferences to be used for notifying
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        notMngr = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void notification() {
        // Check if notifications are enabled
        boolean doNotify = prefs.getBoolean("notifications_new_message", true);

        if (doNotify) {
            // Get data needed to make a notification
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            boolean vibrateNotify = prefs.getBoolean("notifications_new_message_vibrate", true);
            boolean soundNotify = prefs.getBoolean("notifications_new_message_sound", true);

            // Build the notification according to settings
            mBuilder = new NotificationCompat.Builder(mContext)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle(mContext.getResources().getString(R.string.notification_title))
                    .setContentText(mContext.getResources().getString(R.string.notification_text))
                    .setContentIntent(pIntent);
            if (vibrateNotify) {
                mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300});
            }
            if (soundNotify) {
                mBuilder.setSound(uri);
            }

            notMngr.notify(001, mBuilder.build());
        } else {
            cancelAll();
        }
    }

    public void cancelAll() {
        notMngr.cancelAll();
    }
}
